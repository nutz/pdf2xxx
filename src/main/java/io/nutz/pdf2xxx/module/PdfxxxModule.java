package io.nutz.pdf2xxx.module;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Files;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.random.R;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.mvc.upload.TempFile;
import org.nutz.mvc.upload.UploadAdaptor;

import io.swagger.annotations.Api;

@Api("pdf2xxx")
@At("/pdf2xxx")
@IocBean(create = "init", depose = "depose")
public class PdfxxxModule {

    private static final Log log = Logs.get();

    @Inject
    protected PropertiesProxy conf;

    @AdaptBy(type = UploadAdaptor.class)
    @At
    @Ok("raw")
    public File jpg(@Param("file") TempFile tf) throws IOException {
        String dir = "/tmp/pdf2xxx/" + R.UU32();
        File tmpDir = new File(dir);
        Files.createDirIfNoExists(tmpDir);
        File dst = new File(dir + ".zip");
        List<String> args = new ArrayList<>();
        if (Lang.isWin()) {
            args.add("C:\\Program Files\\gs\\gs9.22\\bin\\gswin64c.exe");
        } else {
            args.add("/usr/bin/gs");
        }
        args.add("-dNOPAUSE");
        args.add("-sDEVICE=jpeg");
        args.add("-r200");
        args.add("-dNumRenderingThreads=8");
        args.add("-dJPEGQ=90");
        args.add("-sOutputFile=" + tmpDir.getAbsolutePath() + "/output_%d.jpg");
        args.add(tf.getFile().getAbsolutePath());
        args.add("-dBatch");
        log.debug(Strings.join(" ", args.toArray(new String[args.size()])));
        Lang.execOutput(args.toArray(new String[args.size()]));

        try (FileOutputStream fos = new FileOutputStream(dst)) {
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (int i = 1; i < 1024; i++) {
                String imageName = "output_" + i + ".jpg";
                File output = new File(tmpDir, imageName);
                if (!output.exists())
                    break;
                ZipEntry en = new ZipEntry(imageName);
                zos.putNextEntry(en);
                zos.write(Files.readBytes(output));
                zos.closeEntry();
                output.delete();
            }
            zos.finish();
        }
        // tf.delete();
        return dst;
    }

    public void init() {}

    public void depose() {}

}
